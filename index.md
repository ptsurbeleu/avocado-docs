---
title: Welcome
---

Our platform's mission is to build and foster a community of people who are happy 
users of Jekyll, the awesome static site generator for literally anything. Be it a 
personal or professional blog, marketing page, project documentation, resume, 
portfolio or any other cause why your online content has to exist.

As with any software and Jekyll is no exception, you are expected to be somewhat 
comfortable to speak geek, write geek and be somewhat a geek. However, even that 
might not be enough and at times you need to rollup your sleeves and fix some 
things in your or Jekyll's code. And making code to work your way might be a very 
time-consuming and even intimidating process.

This is where our platform comes to the rescue. We validate, curate, fix, tweak, 
contribute and support themes, plugins or anything for that matter to make your 
static pages work flawlessly, load fast and provide the best possible customer 
support experience ever.

Why? Simply because we feel proud supporting and contributing back to the community 
that already gave us more than enough resources to build and make the project happen. 
We made it #1 priority to treat our customers with exceptional service and support 
and we can't let them down for this very same reason.

Our belief, is that the software should make people life better and the reality is that 
most of the software makes people life miserable for a myriad of reasons... But 
there is a cure for the problem and it is quite simple, someone needs to fix things 
that are broken!

We are fixing it today and you are more than welcome to join us!