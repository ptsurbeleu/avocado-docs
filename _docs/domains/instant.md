---
title: Instant Domain
category: Domains
order: 1
---

Every Jekyll instance created in our system, has a pseudo-random ***.{{ site.instant_suffix }}** domain name that is 
assigned once upon creation and which you can use to instantly access your Jekyll instance.