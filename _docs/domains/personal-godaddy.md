---
title: Personal Domain (GoDaddy)
category: Domains
order: 3
---

If your provider is **GoDaddy**, you can configure corresponding settings in Domain Manager's **DNS Management** section.

Lets see what exactly you need to do to link your *origami.handcraftedby.me* personal domain with your *537de07e.pabloduo.com* Jekyll instance on our side.

Click Domain Manager's [**My Domains**](https://dcc.godaddy.com/manage/){: target="_blank" } link 
to launch domains management tool *(link opens in a new window)*. Once you are signed in and the manager is 
loaded, click **handcraftedby.me**'s Manage button *(with **•••** icon)* to summon context menu.

![My Domains](/images/godaddy_my_domains.png)

In the context menu select *Manage DNS* to change the nitty-gritty details of **handcraftedby.me**'s internals 
and wait for that page to load completely.

**Records** is the section of our interest, because it outlines very imporant domain name settings that we are about to tweak in order to link *origami.handcraftedby.me* and your *537de07e.pabloduo.com* Jekyll instance on our side.

![Records in DNS Management](/images/godaddy_domain_records.png)

Now, scroll down to the bottom of **Records** section to locate **ADD** button and tweak the domain name's internals.

![Add Button in DNS Management](/images/godaddy_add_button.png)

Click **ADD** button and fill out the form fields using the values suggested below and after validating your input, press 
**Save** button to apply these changes.

![New Record in DNS Management](/images/godaddy_new_record.png)

One thing to note, we set **Host** field to *origami* instead of *origami.handcraftedby.me* 
for a good reason - since we are changing internals in the context of **handcraftedby.me** 
domain, there is no need to duplicate that part of the information again.

Finally, we are done with this delicate and very important part linking together *origami.handcraftedby.me* and 
*537de07e.pabloduo.com*.

![New Record in DNS Management](/images/godaddy_done_with_changes.png)

Usually these type of changes are quickly to go live on the Internet. However, if typing 
in your browser *origami.handcraftedby.me*  does not yield the desired results, do not 
fret - give it some time to take these changes into the effect.

> If after an hour or so you still cannot access your Jekyll instance on our side via *origami.handcraftedby.me*,
> do not hesitate to send us a request for help. We are here to help!
{: .warning }