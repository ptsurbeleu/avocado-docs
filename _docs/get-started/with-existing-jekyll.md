---
title: Get Started With Existing Jekyll Site
category: Get Started
order: 1
---

*WORK IN PROGRESS*

If you have an existing Jekyl site, you are more than welcome to use it as is.
We call this feature **Concierge migration** and it is free for all our customers.

Simply click **Start with existing blog** button and follow the wizard, it will
guide you thru steps to initiate your concierge migration.

First of all, we ask you to fill out this form

 - **DOMAIN_NAME** with the domain name of your site, and
 - **SOURCE** with the url of the Git repo where we can find your Jekyll site
 - **BRANCH** with the name of the branch should be used for migration
 - **COMMENTS** with the steps how to build & process your site's custom assets (if any)

then simply click **NEXT** to register your site and the rest we will take care of.

While working on your migration, we might get back to you with additional questions,
in case steps outlined in the comments do not work as expected.

While you are waiting for our system to process the request, you can "entertain" yourself
with finding out nitty-gritty details about what is happening behind the scenes. Click
**SHOW ME GEEKY STUFF** button to expand a panel with the low-level details about what
is happening to your Jekyll site on our end.

Once the process is successfully completed, you should see green **DONE** button to click
and that will get you to the landing page of your Jekyll site where you can find few extra
bits of information about your site.

However, if for some reason the process has encountered an error, you should see blue
**RETRY** and red **HALP** buttons to appear. If that happened to you, do not fret,
hiccups do happen and we will make sure to fix the problem right away. We promise not to
interrogate you with geeky questions, since our system captures enough context in case
of an error to provide insights we need to solve technical issues promptly.

In case you think giving it a another chance might work it out, feel free to click
**RETRY** button to repeat the bootstrapping process again.

However, if you feel you need a helping hand, click red **HALP** button and that will get
you to our support channel right away.

If for any reason you **do** or **do not** like our customer service, please let
us know!